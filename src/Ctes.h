
#ifndef CTES_H_
#define CTES_H_

#include <gdkmm/rgba.h>
#include <gtkmm/image.h>

#define NUM_SOUND_PLAYERS 8

struct Ctes
{
   static const char* VERSION;

   static const char* JSON_FILE;

   // JSON attributes related to the routes structure.
   static const char* ROUTES;
   static const char* ROUTE_NAME;
   static const char* STOPS;
   static const char* STOP_NAME;
   static const char* ELOCUTIONS;
   static const char* ELOC_NAME;
   static const char* ELOC_AUTOPLAY;
   static const char* ELOC_FILES;
   static const char* MUSICS;

   static const char* INDENT_STOPS;
   static const char* INDENT_ELOCS;

   static const char* GPS_FILE;

   // configuration files
   static const char* CONF_FILE_GENERAL_GAIN; // this file just stores an integer with the general gain (100, 200, etc)

   static Gdk::RGBA BACK_COLOR_CURRENT_ELOC;
   static Gdk::RGBA BACK_COLOR_CURRENT_STOP;
   static Gdk::RGBA BACK_COLOR_CURRENT_ROUTE;
   static Gdk::RGBA COLOR_WHITE;
   static Gdk::RGBA COLOR_BLACK;

   static Gtk::Image* mpImgRedCircle_PA;
   static Gtk::Image* mpImgGrayCircle_PA;

   static Gtk::Image* mpImgRedCircle_LiveCH;
   static Gtk::Image* mpImgGrayCircle_LiveCH;

   static Gtk::Image* mpImgRedCircle_GPS;
   static Gtk::Image* mpImgGrayCircle_GPS;

   static Gtk::Image* mpImgRedPauseBtn;
   static Gtk::Image* mpImgGrayPauseBtn;

   static Gtk::Image* mpImgSettings;

   static Gtk::Image* mpImgPlayByGps;
   static Gtk::Image* mpImgPlayByPedal;

   static Gtk::Image* mpImgRoute;
   static Gtk::Image* mpImgPrevComment;
   static Gtk::Image* mpImgNextComment;
   static Gtk::Image* mpImgPrevStop;
   static Gtk::Image* mpImgNextStop;

   static Gtk::Image* mpImgPrevMon;   // for settings window buttons
   static Gtk::Image* mpImgNextMon;
   static Gtk::Image* mpImgPrevPa;
   static Gtk::Image* mpImgNextPa;

   static Gtk::Image* mpImgVolUp;
   static Gtk::Image* mpImgVolDown;

   static Gtk::Image* mpGeneralImgVolUp;
   static Gtk::Image* mpGeneralImgVolDown;
};

#endif /* CTES_H_ */

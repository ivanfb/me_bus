#ifndef GPIO_H_
#define GPIO_H_

#include "Sound.h"

/**
 Sortides BCM27(MSB) a BCM20 (LSB) : Valor 8 bits corresponent al volum del altaveu del equip  (no usem el àudio de la placa)
 Sortides BCM17 a BCM14: Valor 4 bits corresponent al numero de canal (1 a 16) que sortirà pel altaveu del equip.
 Sortida BCM18 := 1 Live Chanel ON
 Sortida BCM13 := 1 PA Chanel ON
 Entrada BCM19 := 1 External play
 */
class Gpio
{
public:
   void start(Sound& aSound);

   void setPA_active(bool aActive);
   bool isPA_active() { return mPA_active; }

   void setLiveCH_active(bool aActive);
   bool isLiveCH_active() { return mLiveCH_active; }

   void setPA_channel(char aPA_channel);

   Glib::Dispatcher& getPA_dispatcher();

private:
   inline void sendCmd(unsigned int aNumGpio, bool aActive);
   void setup_io();

   bool mPA_active;
   bool mLiveCH_active;
};

#endif /* GPIO_H_ */

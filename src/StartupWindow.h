#ifndef STARTUPWINDOW_H_
#define STARTUPWINDOW_H_

#include <gtkmm.h>
#include <gtkmm/window.h>

class StartupWindow : public Gtk::Window {
public:
   StartupWindow(BaseObjectType* cobject,
         const Glib::RefPtr<Gtk::Builder>& refGlade);
   virtual ~StartupWindow();

   void init();

private:
   bool closeWindow();

   Glib::RefPtr<Gtk::Builder>  mBuilder;
};

#endif /* STARTUPWINDOW_H_ */

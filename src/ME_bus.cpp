#include <gtkmm/main.h>
#include <gtkmm/builder.h>
#include <glibmm/fileutils.h>
#include <glibmm/markup.h>

#include "MainWindow.h"
#include "Model.h"
#include "Log.h"
#include "Ctes.h"

int main(int argc, char *argv[])
{
   Log::openLog();

   Log::info("");
   Log::info("==============================================================");
   Log::info("- Starting ME_bus [version - %s]", Ctes::VERSION);
   Log::info("==============================================================");

   try
   {
      Glib::RefPtr<Gtk::Builder> lBuilder;
      auto app = Gtk::Application::create(argc, argv, "metour.app");
      // Parses a file containing a GtkBuilder UI definition. This file was created with 'glade'.
      lBuilder = Gtk::Builder::create_from_file("mtp_linux.glade");

      MainWindow* mpMainWin = nullptr;
      lBuilder->get_widget_derived("mainWindow", mpMainWin);

      app->signal_activate().connect(sigc::mem_fun(*mpMainWin, &MainWindow::init));

      //Shows the window and returns when it is closed.
      app->run(*mpMainWin);
   }
   catch (Glib::FileError& e)
   {
      Log::error("File Error: [%s]", e.what().c_str());
   }
   catch (const Glib::MarkupError& e)
   {
      Log::error("Markup Error: [%s]", e.what().c_str());
   }
   catch (const Gtk::BuilderError& e)
   {
      Log::error("Builder Error: [%s]", e.what().c_str());
   }

   Log::closeLog();

   /* Last thing that main() should do */
   pthread_exit(NULL);
   return 0;
}

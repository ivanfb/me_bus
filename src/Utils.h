
#ifndef UTILS_H_
#define UTILS_H_

#include <pthread.h>

class MutexLocker
{ // this class ensures that the lock-unlock is always done although any
// possible exception in between.
public:
   MutexLocker(pthread_mutex_t &aMux) :
         mMutex(aMux)
   {
      pthread_mutex_lock(&mMutex);
   }

   ~MutexLocker()
   {
      pthread_mutex_unlock(&mMutex);
   }

private:
   pthread_mutex_t &mMutex;
};

#endif /* UTILS_H_ */

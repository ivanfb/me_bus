
#ifndef GPS_H_
#define GPS_H_

#include "Sound.h"

class Gps
{
public:
   void start();

   void setSoundRef(Sound& aSound);

   void setActive(bool aActive);

private:
   bool mIsActive;
};

#endif

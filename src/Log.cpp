#include "Log.h"

#include <gtkmm/messagedialog.h>
#include <syslog.h>

#include "Utils.h"

Gtk::Window* Log::mPtrMainWindow = nullptr;

Log::eLogLevel Log::mLogLevel = Log::LOG_LEVEL_DEBUG;

pthread_mutex_t lockLog;

char mMsg[256];

#define PARSE_INPUT \
      va_list arg; \
      va_start(arg, aMsg); \
      vsnprintf(mMsg, 256, aMsg, arg); \
      va_end(arg);

void Log::openLog()
{
   // look for "metourbus" in /var/log/syslog to see the messages
   openlog("metourbus", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);
   setlogmask (LOG_UPTO (LOG_INFO));
}

// if 'aDebug == true' then the syslog is reopened in 'debug' level. There will be more logs.
void Log::reopenLog(bool aDebug)
{
   closelog();
   openlog("metourbus", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);
   setlogmask (LOG_UPTO (aDebug ? LOG_DEBUG : LOG_INFO));
}

void Log::closeLog()
{
   closelog();
}

void Log::info(const char* aMsg, ...)
{
   MutexLocker muxLocker(lockLog);
   PARSE_INPUT
   syslog (LOG_INFO, mMsg, "");
}

void Log::debug(const char* aMsg, ...)
{
   MutexLocker muxLocker(lockLog);
   PARSE_INPUT
   syslog (LOG_DEBUG, mMsg, "");
}

void Log::error(const char* aMsg, ...)
{
   MutexLocker muxLocker(lockLog);
   PARSE_INPUT
   syslog (LOG_ERR, mMsg, "");

   Gtk::MessageDialog lDialog(*mPtrMainWindow, mMsg, false /* use_markup */,
         Gtk::MESSAGE_ERROR);

   lDialog.run();
}

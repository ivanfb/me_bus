
#ifndef PLAYERS_SOUND_H_
#define PLAYERS_SOUND_H_

#include <string>

class MainWindow;

class Sound
{
public:
   enum eStatus
   {
      TERMINATING_PLAYERS, // when the user press play
      PLAYING,
      PLAYING_MUSIC
   };

   enum ePlayType { NORMAL, GPS, PEDAL };

   Sound();

   void play(ePlayType aType = NORMAL);
   void stop();
   void stopFromPedal();
   void playMusicInAllChannels();

   void setMainwindow(MainWindow* apMainWindow);
   eStatus getStatus() { return mStatus; }

   // the monitor channel can be changed from the Settings Window.
   void playMonitorThread(int aMonitCh = -1);
   void increaseVolumeInMonitor();
   void decreaseVolumeInMonitor();
   void increaseGeneralVolume();
   void decreaseGeneralVolume();

   void playFromGPS();
   void playFromPedal();

private:
   void startPlayAfterPreviousComplete();
   void playFileThread(const std::string& aFile, unsigned int aThId);
   void playMusic(unsigned int aThId);

   void onErrorPlaying(int aThId);

   void handleFinishedThread(int aThId);
   bool on_timeout();
   void killUntilAllPlayersOff();
   int getNumRunningPlayers();

   void persistGeneralGainInFile();

private:
   MainWindow*       mpMainWindow;
   eStatus           mStatus;
   unsigned int      mMonitorChannel;
};

#endif /* PLAYERS_SOUND_H_ */

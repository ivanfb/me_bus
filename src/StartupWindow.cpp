
#include "StartupWindow.h"

#include "Log.h"
#include "MainWindow.h"

StartupWindow::StartupWindow(BaseObjectType* cobject,
      const Glib::RefPtr<Gtk::Builder>& refGlade) :
      Gtk::Window(cobject), mBuilder(refGlade)
{
   set_size_request(800, 480);

   override_background_color(Gdk::RGBA("#ffffff"));

   set_decorated(false); // quit the typical close, minimize buttons.

   Gtk::Image* lpImg = nullptr;
   mBuilder->get_widget("startup_pic", lpImg);
   if (lpImg)
   {
      lpImg->set("/home/pi/ME_bus/imgs/me_logo_big.png");
   }

   show_all();

   sigc::slot<bool> lSlot = sigc::mem_fun(*this, &StartupWindow::closeWindow);
   Glib::signal_timeout().connect(lSlot, 5000 /* milliseconds */);
}

StartupWindow::~StartupWindow() {
}

bool StartupWindow::closeWindow()
{
   close();
   return false; // to stop the timer
}

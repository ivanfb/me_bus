/*
 * SettingsWindow.cpp
 *
 *  Created on: 12 nov. 2017
 *      Author: ivanchi
 */

#include "SettingsWindow.h"

#include "Ctes.h"
#include "Log.h"

SettingsWindow::SettingsWindow(BaseObjectType* cobject,
      const Glib::RefPtr<Gtk::Builder>& refGlade) :
      Gtk::Window(cobject), mBuilder(refGlade),
      mCurrentMonitorChannel(1), mCurrentPaChannel(1)
{
   set_size_request(800, 480);

   set_decorated(false); // quit the typical close, minimize buttons.

   override_background_color(Gdk::RGBA("#ffffff"));

   Gtk::Button* lpButton = nullptr;
   mBuilder->get_widget("btnClose", lpButton);
   if (lpButton)
   {
      lpButton->signal_clicked().connect(
            sigc::mem_fun(*this, &SettingsWindow::on_btn_close_clicked));
   }

   mBuilder->get_widget("btnPrevPaCh", lpButton);
   if (lpButton)
   {
      lpButton->signal_clicked().connect(
            sigc::mem_fun(*this, &SettingsWindow::on_btn_prev_pa_clicked));
      lpButton->set_image(*Ctes::mpImgPrevPa);
      lpButton->set_always_show_image(true);
   }

   mBuilder->get_widget("btnNextPaCh", lpButton);
   if (lpButton)
   {
      lpButton->signal_clicked().connect(
            sigc::mem_fun(*this, &SettingsWindow::on_btn_next_pa_clicked));
      lpButton->set_image(*Ctes::mpImgNextPa);
      lpButton->set_always_show_image(true);
   }

   mBuilder->get_widget("btnPrevMonCh", lpButton);
   if (lpButton)
   {
      lpButton->signal_clicked().connect(
            sigc::mem_fun(*this, &SettingsWindow::on_btn_prev_monitor_clicked));
      lpButton->set_image(*Ctes::mpImgPrevMon);
      lpButton->set_always_show_image(true);
   }

   mBuilder->get_widget("btnNextMonCh", lpButton);
   if (lpButton)
   {
      lpButton->signal_clicked().connect(
            sigc::mem_fun(*this, &SettingsWindow::on_btn_next_monitor_clicked));
      lpButton->set_image(*Ctes::mpImgNextMon);
      lpButton->set_always_show_image(true);
   }

   mBuilder->get_widget("btnMoreVol", lpButton);
   if (lpButton)
   {
      lpButton->signal_clicked().connect(
            sigc::mem_fun(*this, &SettingsWindow::on_btn_more_volume_clicked));
      lpButton->set_image(*Ctes::mpImgVolUp);
      lpButton->set_always_show_image(true);
   }

   mBuilder->get_widget("btnLessVol", lpButton);
   if (lpButton)
   {
      lpButton->signal_clicked().connect(
            sigc::mem_fun(*this, &SettingsWindow::on_btn_less_volume_clicked));
      lpButton->set_image(*Ctes::mpImgVolDown);
      lpButton->set_always_show_image(true);
   }

   mBuilder->get_widget("lblMonitorChannel", mpMonChannel);
   mBuilder->get_widget("lblPaChannel", mpPaChannel);

   mBuilder->get_widget("btnMoreGeneralVol", lpButton);
   if (lpButton)
   {
      lpButton->signal_clicked().connect(
            sigc::mem_fun(*this, &SettingsWindow::on_btn_more_general_volume_clicked));
      lpButton->set_image(*Ctes::mpGeneralImgVolUp);
      lpButton->set_always_show_image(true);
   }

   mBuilder->get_widget("btnLessGeneralVol", lpButton);
   if (lpButton)
   {
      lpButton->signal_clicked().connect(
            sigc::mem_fun(*this, &SettingsWindow::on_btn_less_general_volume_clicked));
      lpButton->set_image(*Ctes::mpGeneralImgVolDown);
      lpButton->set_always_show_image(true);
   }

   showNumPaChannel();
   showNumMonitorChannel();
}

SettingsWindow::~SettingsWindow()
{
}

void SettingsWindow::setSoundRef(Sound& aSound)
{
   mpSound = &aSound;
}

void SettingsWindow::setGpioRef(Gpio& aGpio)
{
   mpGpio = &aGpio;
}

void SettingsWindow::on_btn_close_clicked()
{
   close();
}

void SettingsWindow::on_btn_prev_pa_clicked()
{
   if (mCurrentPaChannel > 1)
   {
      mCurrentPaChannel--;
   }

   mpGpio->setPA_channel((char) mCurrentPaChannel - 1);
   showNumPaChannel();
}

void SettingsWindow::on_btn_next_pa_clicked()
{
   if (mCurrentPaChannel < (NUM_SOUND_PLAYERS * 2) )
   {
      mCurrentPaChannel++;
   }

   mpGpio->setPA_channel((char) mCurrentPaChannel - 1);
   showNumPaChannel();
}

void SettingsWindow::on_btn_prev_monitor_clicked()
{
   if (mCurrentMonitorChannel > 1)
   {
      mCurrentMonitorChannel--;
   }

   if (mpSound)
   {
      mpSound->playMonitorThread(mCurrentMonitorChannel - 1);
   }

   showNumMonitorChannel();
}

void SettingsWindow::on_btn_next_monitor_clicked()
{
   if (mCurrentMonitorChannel < (NUM_SOUND_PLAYERS * 2))
   {
      mCurrentMonitorChannel++;
   }

   if (mpSound)
   {
      mpSound->playMonitorThread(mCurrentMonitorChannel - 1);
   }

   showNumMonitorChannel();
}

void SettingsWindow::showNumPaChannel()
{
   if (mpPaChannel != nullptr)
   {
      char lNum[3];
      snprintf(lNum, 3, "%u", mCurrentPaChannel);
      mpPaChannel->set_text(lNum);
   }
}

void SettingsWindow::showNumMonitorChannel()
{
   if (mpMonChannel != nullptr)
   {
      char lNum[3];
      snprintf(lNum, 3, "%u", mCurrentMonitorChannel);
      mpMonChannel->set_text(lNum);
   }
}

void SettingsWindow::on_btn_more_volume_clicked()
{
   if (mpSound != nullptr)
   {
      mpSound->increaseVolumeInMonitor();
   }
}

void SettingsWindow::on_btn_less_volume_clicked()
{
   if (mpSound != nullptr)
   {
      mpSound->decreaseVolumeInMonitor();
   }
}

void SettingsWindow::on_btn_more_general_volume_clicked()
{
   if (mpSound != nullptr)
   {
      mpSound->increaseGeneralVolume();
   }
}

void SettingsWindow::on_btn_less_general_volume_clicked()
{
   if (mpSound != nullptr)
   {
      mpSound->decreaseGeneralVolume();
   }
}



#ifndef MODEL_H_
#define MODEL_H_

/**
 * This class is aimed to handle the data model which gets constructed from a
 * JSON file.
 */
class Model
{
public:

   struct Route; // forward declarations to fix circular references in structures
   struct Stop;

   // struct that represents the pointer to current route-stop-comment
   struct BookMark
   {
      unsigned int mRoute;
      unsigned int mStop;
      unsigned int mEloc;

      BookMark(int aR, int aS, int aE) : mRoute(aR), mStop(aS), mEloc(aE){}
      void setBookMark(int aR, int aS, int aE)
      {
         mRoute = aR; mStop = aS; mEloc = aE;
      }
   };

   // To keep gps information of Elocutions with gps.
   struct LocucioGPS
   {
      // Lat and Lon margins are in seconds
      double marge_lat, marge_long, direccio, marge_direccio;

      // Lat and Lon are stored in seconds to simplify calculus
      double latitud, longitud;

      char NS, WE;

      LocucioGPS(double aMargeLat, double aMargeLong,
                 double aDireccio, double aMargeDireccio,
                 double aLatitud, double aLongitud, char aNS, char aWE)
      :
         marge_lat(aMargeLat), marge_long(aMargeLong), direccio(aDireccio),
         marge_direccio(aMargeDireccio), latitud(aLatitud), longitud(aLongitud),
         NS(aNS), WE(aWE)
      {}
   };

   struct Elocution
   {
      Elocution(const std::string& aElocName,
                unsigned int aElocIndex,
                unsigned int aAbsElocIndex,
                Stop* apParent,
                bool aAutoplay) :
         mElocutionName((aAutoplay ? "- A - ":"") + aElocName),
         mElocIndex(aElocIndex),
         mNumEloc(aElocIndex + 1), mAbsElocIndex(aAbsElocIndex), mAutoplay(aAutoplay),
         mpParent(apParent),
         mpGpsInfo(nullptr)
      {}

      bool isCurrent() const
      {
         return mBookMark.mEloc == (mNumEloc - 1); // numEloc starts by '1'.
      }

      std::vector<std::string> mAudioFiles;
      std::string mElocutionName;
      unsigned int mElocIndex;
      unsigned int mNumEloc;
      unsigned int mAbsElocIndex;
      bool mAutoplay;

      Stop* mpParent;
      LocucioGPS *mpGpsInfo;
   };

   struct Stop
   {
      Stop(const std::string& aStopName, unsigned int aStopIndex, Route* apParent) :
         mStopName(aStopName), mStopIndex(aStopIndex),
         mNumStop(aStopIndex + 1), mpParent(apParent) {}
      ~Stop()
      {
         for (Elocution* e : mElocs)
         {
            delete (e);
         }
         mElocs.clear();
      }

      std::vector<std::string> mMusics;
      std::vector<Elocution *> mElocs;
      std::string mStopName;
      unsigned int mStopIndex;
      unsigned int mNumStop;
      Route* mpParent;
   };

   struct Route
   {
      Route(const std::string& aRouteName, unsigned int aRouteIndex) :
            mRouteName(aRouteName), mRouteIndex(aRouteIndex) {}
      ~Route()
      {
         for (Stop* s : mStops)
         {
            delete (s);
         }
         mStops.clear();
      }

      std::string mRouteName;
      unsigned int mRouteIndex;
      std::vector<Stop *> mStops;
   };

   typedef std::vector<Route *> VectorRoutes;
   typedef std::vector<Elocution *> VectorElocs;

   // enum that delimits the number of jumps when looking for prev, or prev-prev, etc
   enum eNumJumps
   {
      ONE = 1,
      TWO
   };

   // to allow look backward or forward for previous or next elocution
   enum ePrevNext
   {
      PREV, NEXT
   };

   virtual ~Model();

   // Parses the JSON file and load its content into memory
   static void parse();

   // Return the routes vector
   static const std::vector<Route* >& getRoutes() { return mRoutes; }

   static void setCurrentStop(int aStop);
   static void setCurrentEloc(int aEloc);

   static const Route* getCurrentRoute();
   static const Stop* getPrevStop();
   static const Stop* getCurrentStop();
   static const Stop* getNextStop();
   static const Elocution* getPrevNextEloc(ePrevNext aPrevNext,
                                           eNumJumps aNumJumps);
   static const Elocution* getCurrentEloc();
   static const std::vector<Elocution *>& getCurrentElocsList();

   static void goNextStop();
   static void goPrevStop();
   static void goNextEloc();
   static void goPrevEloc();
   static void goNextRoute();

   static void goNextElocInRoute(); // this is needed for 'autoplay' purposes.

   static void getCommentAudioAtChannel(unsigned int aNumChannel,
                                        std::string& aAudioFile);
   static const std::vector<std::string>& getCurrentMusics();

   static void getRangeElocsForGps(VectorElocs::const_iterator& aFirst,
                                   VectorElocs::const_iterator& aEnd);

   static bool gpsMatch(double aGpsLat, double aGpsLon, double aGpsOri,
                        char aNS, char aWE);

private:
   static void moveBookMarkToEloc(const Elocution& aEloc);

   // Checks that route, stop and eloc from posgps.inf are in json model
   static bool checkExists(unsigned int aRoute,
                           unsigned int aStop, unsigned int aEloc);

   // parses the posgps.info file and loads its content into memory
   static void parseGpsInfo();
   static int mMarginGpsElocs; // maximum level of elocution loss for the GPS

   // Calculates how many seconds one grade represents in terms of latitude and longitude
   static void midaSegonsGPS(double latitud, double *metres_lat, double *metres_lon);

   static VectorRoutes mRoutes;
   static BookMark mBookMark; // stores the current position in route,stop,eloc
   static VectorElocs mAllElocs; // for GPS purposes

   // obj null pattern to avoid returning nullptr's
   static Route* mpNullRoute;
   static Stop* mpNullStop;
   static Elocution* mpNullEloc;

   static bool mGpsActive;
};

#endif /* MODEL_H_ */

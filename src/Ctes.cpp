#include "Ctes.h"

const char* Ctes::VERSION = "0.0.16";

const char* Ctes::JSON_FILE = "/home/pi/ME_bus/data/project.json";

const char* Ctes::ROUTES = "Routes";
const char* Ctes::ROUTE_NAME = "RouteName";
const char* Ctes::STOPS = "Stops";
const char* Ctes::STOP_NAME = "StopName";
const char* Ctes::ELOCUTIONS = "Elocutions";
const char* Ctes::ELOC_NAME = "ElocutionName";
const char* Ctes::ELOC_AUTOPLAY = "IsAutoplay";
const char* Ctes::ELOC_FILES = "ElocutionFiles";
const char* Ctes::MUSICS = "Musics";

const char* Ctes::INDENT_STOPS = "   ";
const char* Ctes::INDENT_ELOCS = "         ";

const char* Ctes::GPS_FILE = "/home/pi/ME_bus/data/posgps.inf";

const char* Ctes::CONF_FILE_GENERAL_GAIN = "/home/pi/general_gain";

Gdk::RGBA Ctes::BACK_COLOR_CURRENT_ELOC("#B03060");
Gdk::RGBA Ctes::BACK_COLOR_CURRENT_STOP("#B0B060");
Gdk::RGBA Ctes::BACK_COLOR_CURRENT_ROUTE("#B000FF");
Gdk::RGBA Ctes::COLOR_WHITE("#ffffff");
Gdk::RGBA Ctes::COLOR_BLACK("#000000");

// each toogle button should have its own image object
Gtk::Image* Ctes::mpImgRedCircle_PA = new Gtk::Image("/home/pi/ME_bus/imgs/red.png");
Gtk::Image* Ctes::mpImgGrayCircle_PA = new Gtk::Image("/home/pi/ME_bus/imgs/gray.png");

Gtk::Image* Ctes::mpImgRedCircle_LiveCH = new Gtk::Image("/home/pi/ME_bus/imgs/red.png");
Gtk::Image* Ctes::mpImgGrayCircle_LiveCH = new Gtk::Image("/home/pi/ME_bus/imgs/gray.png");

Gtk::Image* Ctes::mpImgRedCircle_GPS = new Gtk::Image("/home/pi/ME_bus/imgs/red.png");
Gtk::Image* Ctes::mpImgGrayCircle_GPS = new Gtk::Image("/home/pi/ME_bus/imgs/gray.png");

Gtk::Image* Ctes::mpImgRedPauseBtn = new Gtk::Image("/home/pi/ME_bus/imgs/red.png");
Gtk::Image* Ctes::mpImgGrayPauseBtn = new Gtk::Image("/home/pi/ME_bus/imgs/gray.png");

Gtk::Image* Ctes::mpImgSettings = new Gtk::Image("/home/pi/ME_bus/imgs/settings_icon.png");

Gtk::Image* Ctes::mpImgPlayByGps = new Gtk::Image("/home/pi/ME_bus/imgs/play_by_gps.png");
Gtk::Image* Ctes::mpImgPlayByPedal = new Gtk::Image("/home/pi/ME_bus/imgs/play_by_pedal.png");

Gtk::Image* Ctes::mpImgRoute = new Gtk::Image("/home/pi/ME_bus/imgs/btn_route.png");
Gtk::Image* Ctes::mpImgPrevComment = new Gtk::Image("/home/pi/ME_bus/imgs/prev_comment.png");
Gtk::Image* Ctes::mpImgNextComment = new Gtk::Image("/home/pi/ME_bus/imgs/next_comment.png");
Gtk::Image* Ctes::mpImgPrevStop = new Gtk::Image("/home/pi/ME_bus/imgs/prev_stop.png");
Gtk::Image* Ctes::mpImgNextStop = new Gtk::Image("/home/pi/ME_bus/imgs/next_stop.png");

Gtk::Image* Ctes::mpImgPrevMon = new Gtk::Image("/home/pi/ME_bus/imgs/next.png");
Gtk::Image* Ctes::mpImgNextMon = new Gtk::Image("/home/pi/ME_bus/imgs/prev.png");
Gtk::Image* Ctes::mpImgPrevPa = new Gtk::Image("/home/pi/ME_bus/imgs/next.png");
Gtk::Image* Ctes::mpImgNextPa = new Gtk::Image("/home/pi/ME_bus/imgs/prev.png");

Gtk::Image* Ctes::mpImgVolUp = new Gtk::Image("/home/pi/ME_bus/imgs/vol_up.png");
Gtk::Image* Ctes::mpImgVolDown = new Gtk::Image("/home/pi/ME_bus/imgs/vol_down.png");

Gtk::Image* Ctes::mpGeneralImgVolUp = new Gtk::Image("/home/pi/ME_bus/imgs/vol_up.png");
Gtk::Image* Ctes::mpGeneralImgVolDown = new Gtk::Image("/home/pi/ME_bus/imgs/vol_down.png");



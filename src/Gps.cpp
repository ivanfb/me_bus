#include "Gps.h"
#include "Log.h"
#include "Model.h"

#include <gps.h>
#include <libgpsmm.h>
#include <glibmm.h>

#include <iostream>
#include <pthread.h>

#define MIN_SPEED 3 // minimum speed to activate GPS trigger

pthread_t mTh;

Sound* mpSound;

bool mIsActive = false;

void* getPos(void*)
{
   int lNumAttempt = 0, lMaxAttemptsWithoutData = 200;

   Log::info("Gps: starting getPos");

   struct gps_data_t data;

retry:
   int i = gps_open("localhost", DEFAULT_GPSD_PORT, &data);
   if (i == 0)
   {
      gps_stream(&data, WATCH_ENABLE | WATCH_JSON, NULL);

      while (true)
      {
         if (mIsActive)
         {
            if (gps_waiting(&data, 2000000))
            {
               errno = 0;

               if (gps_read(&data) > 0)
               {
                  if (!isnan(data.fix.latitude) && !isnan(data.fix.longitude))
                  { // there is valid position data
                     double lSpeed = data.fix.speed * 3.6; // m/s into km/h
                     if (lSpeed >= MIN_SPEED)
                     {
                        if (mpSound != nullptr)
                        {
                           // Check that it's playing music. It's allowed to play.
                           if (mpSound->getStatus() == Sound::PLAYING_MUSIC)
                           {
                              if (Model::gpsMatch(data.fix.latitude,
                                    data.fix.longitude,
                                    data.fix.track, // direction/orientation
                                    data.fix.latitude >= 0 ? 'N' : 'S',
                                    data.fix.longitude >= 0 ? 'E' : 'W'))
                              {
                                 mpSound->playFromGPS();
                              }
                           }
                        }
                     }

                     Log::debug("GPS info [lat %f][lon %f][direction %f]",
                           data.fix.latitude, data.fix.longitude,
                           data.fix.track);
                     lNumAttempt = 0;
                  }
               }

               lNumAttempt++;
            }

            if (lNumAttempt > lMaxAttemptsWithoutData) // lNumAttempt wasn't resetted for a long time.
            {
               lNumAttempt = 0;
               Log::info(
                     "Gps: Max attempts [%d] reached, restarting connection",
                     lNumAttempt);

               gps_stream(&data, WATCH_DISABLE, NULL);
               gps_close(&data);

               goto retry;
            }
         } // if (mIsActive)

         sleep (3 /* seconds */);
      }
   }
   else
   {
      Log::info("Gps: ERROR [%s]", gps_errstr(errno));
   }

   return nullptr;
}

void Gps::setSoundRef(Sound& aSound)
{
   mpSound = &aSound;
}

void Gps::start()
{
   int rc = pthread_create(&mTh, nullptr, getPos, nullptr);
   if (rc)
   {
      Log::error("Sound: ERROR return code from pthread_create() is %d\n", rc);
   }
}

void Gps::setActive(bool aActive)
{
   mIsActive = aActive;
}

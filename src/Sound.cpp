#include "Sound.h"

#include <glibmm/dispatcher.h>

#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#include <sys/time.h>
#include <sstream>
#include <pthread.h>
#include "semaphore.h"

#include "MainWindow.h"
#include "Model.h"
#include "Log.h"
#include "Utils.h"
#include "Ctes.h"

bool Model::mGpsActive = false;

#define MAX_VOLUM_LIMIT_DBS 900
#define STEP_MILIBELS 300

#define GAIN_STEP 100

// counts the number of times the user pressed '+' or '-' to increase/decrease
// the volume. The purpose of this variable is to keep the same volume
// in monitor channel between consecutive reproductions.
int mMonitorVolume = 0;

int mGeneralMpg321Gain = 100; // gain to be passed to the mpg321 program

FILE* mpMonitorKey = nullptr; // used to send keys to monitor player

pthread_t mMonitTh; // thread that plays the monitor channel.
bool mMonitThRunning = false;

struct sThread
{
   pthread_t mTh;
   std::string mFile; // file to play
   unsigned int mId;
   bool mThJoined; // true if thread has been joined by the parent thread
   bool mThPlayingMusic; // true if thread is playing music
   unsigned int mIndexMusic; // 0..num_music_files - 1
   std::string mErrorMsg;
   Glib::Dispatcher mErrorDispatcher; // to notify errors to the main thread.
   Glib::Dispatcher mEndDispatcher; // to notify when a thread ends.
   timeval mStartTime; // to measure the playing time. Needed by audio monitor.
};

// to allow play from GPS and play/stop from Pedal threads
Glib::Dispatcher mPlayGps, mPlayPedal, mStopPedal;

sThread mThreads[NUM_SOUND_PLAYERS];

std::vector<std::string> mMusicFiles; // current list of music files

// For synchronizing threads with the main thread and make all threads start
// simultaneously.
sem_t mSem;

pthread_mutex_t lock;

Sound::Sound() : mpMainWindow(nullptr),
                 mStatus(Sound::PLAYING_MUSIC),
                 mMonitorChannel(0)
{
   sem_init(&mSem, 0, 1); // non-shared semaphore with initial value '1'.

   // Connect the handlers to the dispatchers.
   for (int i = 0; i < NUM_SOUND_PLAYERS; i++)
   {
      mThreads[i].mEndDispatcher
          .connect(sigc::bind<int>(
                sigc::mem_fun(*this, &Sound::handleFinishedThread), i /*thread id*/));

      mThreads[i].mErrorDispatcher
          .connect(sigc::bind<int>(
                sigc::mem_fun(*this, &Sound::onErrorPlaying), i /*thread id*/));
   }

   // connecting dispatchers to 'play' method. Changes in GUI MUST be done by
   // the main thread. No other thread can change any icon, etc.
   mPlayGps.connect(sigc::bind<Sound::ePlayType>(sigc::mem_fun(*this, &Sound::play), Sound::GPS));
   mPlayPedal.connect(sigc::bind<Sound::ePlayType>(sigc::mem_fun(*this, &Sound::play), Sound::PEDAL));
   mStopPedal.connect(sigc::mem_fun(*this, &Sound::stop));

   // read the general gain value, if any
   std::fstream lGainFile(Ctes::CONF_FILE_GENERAL_GAIN, std::ios_base::in);
   if(!lGainFile.fail()){
      lGainFile >> mGeneralMpg321Gain;
   }
}

// Handles the error messages send by the threads.
void Sound::onErrorPlaying(int aThId)
{
   Log::error(mThreads[aThId].mErrorMsg.c_str());
   handleFinishedThread(aThId);
}

void Sound::handleFinishedThread(int aThId)
{
   MutexLocker muxLocker(lock);
   Log::debug("Sound: handleFinishedThread [%u]", aThId);

   // count the number of threads that are playing music
   // in 'PLAYING' status, some threads play music if they ended the comment.
   int lNumThreadsPlayingMusic = 0;
   int lNumJoinedThreads = 0;
   for (int i = 0; i < NUM_SOUND_PLAYERS; i++)
   {
      if (mThreads[i].mThPlayingMusic)
      {
         lNumThreadsPlayingMusic++;
      }

      if (mThreads[i].mThJoined)
      {
         lNumJoinedThreads++;
      }
   }

   if (!mThreads[aThId].mThJoined)
   {
      Log::debug("Sound: joining thread [%u]", aThId);

      // join thread that has ended and wasn't joined yet.
      pthread_join(mThreads[aThId].mTh, nullptr);
      mThreads[aThId].mThJoined = true;

      if (mStatus == TERMINATING_PLAYERS)
      {
         if (lNumJoinedThreads == (NUM_SOUND_PLAYERS - 1))
         { // this is the last joined thread
            Log::debug("Sound: playing after joining previous threads");
            startPlayAfterPreviousComplete();
         }
      }
      else if (mStatus == PLAYING)
      {
         if (lNumThreadsPlayingMusic == (NUM_SOUND_PLAYERS - 1))
         {  // This is the thread that played the longest elocution
            bool lIsAutoplay = Model::getCurrentEloc()->mAutoplay;

            // go to next elocution
            Model::goNextElocInRoute();
            mpMainWindow->showRoutes();
            mMusicFiles = Model::getCurrentMusics(); // update musics for current stop

            if ( mpMainWindow->isPausePressed() || !lIsAutoplay )
            {
               // all threads are playing music now
               mStatus = Sound::PLAYING_MUSIC;
               Log::debug("Sound: playing music not autoplay or pause pressed");
               playMusic(mThreads[aThId].mId);
               mpMainWindow->setGpsIconVisible(false); // always hide the gps icon when done
               mpMainWindow->setPedalIconVisible(false);
            }
            else
            {
               Log::debug("Sound: Autoplay");
               // call this to stop the other threads that are playing music
               play(); // By this the commentaries will start
            }

            if (mpMainWindow->isPausePressed())
            {
               Log::debug("Sound: unchecking autoplay");
               mpMainWindow->unchekPauseButton();
            }
         }
         else
         {
            Log::debug("Sound: playing music not last thread");
            playMusic(mThreads[aThId].mId);
         }
      }
      else if (mStatus == PLAYING_MUSIC) // keep playing music
      {
         Log::debug("Sound: playing music thread [%u] PLAYING_MUSIC", mThreads[aThId].mId);
         playMusic(mThreads[aThId].mId);
      }
   }
}

void Sound::setMainwindow(MainWindow* apMainWindow)
{
   mpMainWindow = apMainWindow;
}

bool Sound::on_timeout()
{
   if (mStatus == Sound::PLAYING)
   {
      mpMainWindow->updateProgress();
      return true; // keep updating the time and progress bar
   }
   else
   {
      mpMainWindow->stopPlayProgress();
      return false; // stop the timer
   }
}

// thread that plays the audio monitor
void* runMonitorPlay(void *args)
{
   mMonitThRunning = true;

   // the channel to be monitored. From 0 to NUM_SOUND_PLAYERS - 1
   unsigned int lChannelToMonitor = *((unsigned int *) args);

   int lSoundPlayerToMonitor = lChannelToMonitor % NUM_SOUND_PLAYERS;

//  -0 Decode only channel 0 (left)
//  -1 Decode only channel 1 (right).  These options are available for stereo MPEG streams only.
   const char* lChannelSelector = (lChannelToMonitor < NUM_SOUND_PLAYERS) ? " -0 ":" -1 ";

   timeval lNow;
   gettimeofday (&lNow, NULL) ;
   int lNumSeconds = lNow.tv_sec - mThreads[lSoundPlayerToMonitor].mStartTime.tv_sec;

   // playing the same file from the current playing second
   std::ostringstream cmd;
   cmd << "mpg123 -w aux.wav " << lChannelSelector << " "
       << mThreads[lSoundPlayerToMonitor].mFile
       << " && omxplayer --no-osd -l " << lNumSeconds << " "
       << " --vol " << mMonitorVolume << " aux.wav > /dev/null 2>&1";

   if (mpMonitorKey != nullptr)
   {
      fclose(mpMonitorKey);
      mpMonitorKey = nullptr;
   }

   mpMonitorKey = popen(cmd.str().c_str(), "w");
   pthread_exit(NULL);
   mMonitThRunning = false;

   return nullptr;
}

// Threads function
void* runPlay(void *args)
{
   unsigned int lThId = *((unsigned int *) args);

   std::ostringstream cmd;
   cmd << "mpg321 -g " << mGeneralMpg321Gain << " -o alsa -a hw:Device";

   if (lThId > 0)
   {
      switch (lThId)
      {
      case 14:
         cmd << std::string("_E");
         break;
      case 15:
         cmd << std::string("_F");
         break;
      default:
         cmd << std::string("_") << std::to_string(lThId);
         break;
      }
   }

   cmd << ",0 " <<  mThreads[lThId].mFile << " > /dev/null 2>&1";

   sem_post(&mSem); // leave the semaphore to allow the main thread create another thread.

   mThreads[lThId].mThJoined = false;

   Log::debug("Sound: thread [%u] running [%s]", lThId, cmd.str().c_str());
   int lRet = system(cmd.str().c_str());

   {
      MutexLocker muxLocker(lock);

      Log::debug("Sound: thread [%u] returned [%u]", lThId, lRet);

      std::stringstream ss;
      switch (lRet)
      {
         case 1:
         {
            Log::debug("Sound: void* run -> thread [%u] file not found", lThId);
            ss << "File not found in thread " << lThId << " command: " << cmd.str();
            mThreads[lThId].mErrorMsg = ss.str();
            mThreads[lThId].mErrorDispatcher.emit();
         }
            break;
         case 0:
            // the process exited correctly. i.e. the elocution/music was completed.
            Log::debug("Sound: void* run -> thread [%u] finished normally", lThId);
            mThreads[lThId].mEndDispatcher.emit();
            break;
         default:
            // the process was killed intentionally.
            Log::debug("Sound: void* run -> thread [%u] finished by TERM request", lThId);
            mThreads[lThId].mEndDispatcher.emit();
            break;
      }

      pthread_exit(NULL);
   }

   return nullptr;
}

void Sound::playFileThread(const std::string& aFile, unsigned int aThId)
{
   mThreads[aThId].mErrorMsg = std::string(); // empty the error message
   mThreads[aThId].mId = aThId;

   // so far, the maximum num of channels was reached with mp3.
   mThreads[aThId].mFile = aFile;

   gettimeofday(&mThreads[aThId].mStartTime, NULL); // start counting time. Needed by channel monitor.

   Log::debug("Sound: void* run -> sem_wait thread [%u]", aThId);
   sem_wait(&mSem);
   Log::debug("Sound: creating thread [%u]\n", mThreads[aThId].mId);
   int rc = pthread_create(&mThreads[aThId].mTh, nullptr, runPlay, &mThreads[aThId].mId);
   if (rc)
   {
      Log::error("Sound: ERROR return code from pthread_create() is %d\n", rc);
      exit(-1);
   }
   else if ((mMonitorChannel % NUM_SOUND_PLAYERS) == aThId)
   {  // The '%' is needed because each sound player reproduces two channels.
      // e.g.: channel 0 and 8; or channel 3 and 11 (considering NUM_SOUND_PLAYERS == 8.
      playMonitorThread();
   }
}

void Sound::playMonitorThread(int aMonitCh)
{
   if (aMonitCh != -1)
   {
      mMonitorChannel = aMonitCh;
   }

   Log::debug("Sound: monitoring channel [%u]\n", mMonitorChannel);

   // this thread should be running always
   if (mMonitThRunning)
   {
      system("pkill -TERM -f aux.wav"); // killing the mpg123 and omxplayer of monitor channel
      pthread_join(mMonitTh, nullptr);
   }

   int rc = pthread_create(&mMonitTh, nullptr, runMonitorPlay, &mMonitorChannel);
   if (rc)
   {
      Log::error("Sound: ERROR in playMonitorThread. Return code [%d]\n", rc);
      exit(-1);
   }
}

void Sound::increaseVolumeInMonitor()
{
   Log::debug("Sound: increase monitor volume");
   if (mpMonitorKey != nullptr)
   {
      if (mMonitorVolume < MAX_VOLUM_LIMIT_DBS)
      {
         mMonitorVolume += STEP_MILIBELS;
         fputs("+", mpMonitorKey); // sending '+' key to omxplayer to increase vol
         fflush(mpMonitorKey);
      }
   }
}

void Sound::decreaseVolumeInMonitor()
{
   Log::debug("Sound: decrease monitor volume");
   if (mpMonitorKey != nullptr)
   {
      mMonitorVolume -= STEP_MILIBELS;
      fputs("-", mpMonitorKey); // sending '-' key to omxplayer to decrease vol
      fflush(mpMonitorKey);
   }
}

void Sound::increaseGeneralVolume()
{
   mGeneralMpg321Gain += GAIN_STEP;
   killUntilAllPlayersOff();
   persistGeneralGainInFile();
}

void Sound::decreaseGeneralVolume()
{
   if (mGeneralMpg321Gain > 0)
   {
      mGeneralMpg321Gain -= GAIN_STEP;
      killUntilAllPlayersOff();
      persistGeneralGainInFile();
   }
}

void Sound::persistGeneralGainInFile()
{
   // write the general gain value into conf file
   std::fstream lGainFile(Ctes::CONF_FILE_GENERAL_GAIN, std::ios_base::out);
   if(!lGainFile.fail()){
      lGainFile << mGeneralMpg321Gain;
   }
}

void Sound::play(ePlayType aType)
{
   if (mStatus != TERMINATING_PLAYERS)
   {
      mStatus = TERMINATING_PLAYERS;

      if (aType == Sound::GPS)
      {
         Log::info("Sound: playFromGPS");
         mpMainWindow->setGpsIconVisible(true);
      }
      else if (aType == Sound::PEDAL)
      {
         Log::info("Sound: playFromPedal");
         mpMainWindow->setPedalIconVisible(true);
      }
      else
      {
         Log::info("Sound: play");
      }

      killUntilAllPlayersOff();
   }
}

// This method should not be called directly. Only by 'handleFinishedThread'
void Sound::startPlayAfterPreviousComplete()
{
   Log::info("Sound: startPlayAfterPreviousComplete");

   mpMainWindow->showRoutes(); // repaint the routes after moving the book mark

   mStatus = Sound::PLAYING;

   int lMaxSecs = 0;
   char lNumSecs[8];
   std::string lFile;

   std::ostringstream lCmd;
   for (unsigned int i = 0; i < NUM_SOUND_PLAYERS; i++)
   {
      mThreads[i].mThPlayingMusic = false;
      Model::getCommentAudioAtChannel(i, lFile); // get files of current eloc
      playFileThread(lFile, i);

      lCmd << "mp3info -p %S ";
      lCmd << lFile;

      // the call to mp3info returns the file's duration in seconds
      FILE* lRetCommand = popen(lCmd.str().c_str(), "r");
      if (!lRetCommand)
      {
         Log::error("Sound: could not open pipe for output [%s]", lCmd.str().c_str());
         return;
      }

      fgets(lNumSecs, 8, lRetCommand);

      int lIntSecs = atoi(lNumSecs);
      if (lIntSecs > lMaxSecs)
      {
         lMaxSecs = lIntSecs;
      }

      pclose(lRetCommand);
   }

   mpMainWindow->startPlayProgress(lMaxSecs);

   // aimed to paint time and progress bar
   sigc::slot<bool> lSlot = sigc::mem_fun(*this, &Sound::on_timeout);
   Glib::signal_timeout().connect(lSlot, 1000 /* 1 second */);
}

// This is called at the start up of the app. It's considered to be stopped
// with music in background.
void Sound::playMusicInAllChannels()
{
   Log::debug("Sound: playMusicInAllChannels");

   mStatus = PLAYING_MUSIC;
   mMusicFiles = Model::getCurrentMusics();
   for (int i = 0; i < NUM_SOUND_PLAYERS; i++)
   {
      playMusic(i);
   }
}

void Sound::playMusic(unsigned int aThId)
{
   Log::debug("Sound: playMusic thread [%u]", aThId);

   mThreads[aThId].mThPlayingMusic = true;

   unsigned int lCurrentMusicFileIndex = mThreads[aThId].mIndexMusic;

   if (lCurrentMusicFileIndex >= mMusicFiles.size())
   {
      lCurrentMusicFileIndex = 0; // this is to avoid possible buffer overrun
   }

   playFileThread(mMusicFiles[lCurrentMusicFileIndex], aThId);

   // update music index for next time.
   mThreads[aThId].mIndexMusic++;
   if (mThreads[aThId].mIndexMusic >= mMusicFiles.size())
   {
      mThreads[aThId].mIndexMusic = 0;
   }
}

void Sound::stop()
{
   if (mStatus == Sound::PLAYING)
   {
      Log::info("Sound: stop");
      mStatus = Sound::PLAYING_MUSIC;
      mpMainWindow->setGpsIconVisible(false); // ensure to hide gps icon
      mpMainWindow->setPedalIconVisible(false);
      mpMainWindow->unchekPauseButton();
      killUntilAllPlayersOff();
   }
}

void Sound::stopFromPedal()
{
   Log::info("Sound: stopFromPedal");
   mStopPedal.emit();
}

// This method is needed to ensure that all the players will get killed.
// It could happen that a "kill" order arrives before all channels are
// playing something.
void Sound::killUntilAllPlayersOff()
{
   for (int i = 0; i < 400 && getNumRunningPlayers() != 0; i++)
   {
      usleep(10000);
      system("pkill -TERM mpg321");
   }
}

int Sound::getNumRunningPlayers()
{
   char lNumRunning[8];
   FILE* lRetCommand = popen("pgrep mpg321 | wc -l", "r");
   if (!lRetCommand)
   {
      Log::error("Sound: could not open pipe for output in getNumRunningPlayers");
      return 0;
   }

   fgets(lNumRunning, 8, lRetCommand);
   fclose(lRetCommand);

   return atoi(lNumRunning);
}

void Sound::playFromGPS()
{
   mPlayGps.emit();
}

void Sound::playFromPedal()
{
   mPlayPedal.emit();
}

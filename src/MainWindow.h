#ifndef _MAIN_WINDOW_H
#define _MAIN_WINDOW_H

#include <gtkmm.h>
#include <gtkmm/window.h>

#include "Sound.h"
#include "Gps.h"
#include "Gpio.h"
#include "SettingsWindow.h"

#define NUM_ROWS 9

class MainWindow: public Gtk::Window
{
public:
   MainWindow(BaseObjectType* cobject,
         const Glib::RefPtr<Gtk::Builder>& refGlade);
   virtual ~MainWindow();

   void showRoutes(); // needed by 'Sound' when for moving to next eloc.
   void init();
   void startPlayProgress(int aNumSeconds);
   void updateProgress();
   void stopPlayProgress();
   void unchekPauseButton();

   void setGpsIconVisible(bool aVisible);
   void setPedalIconVisible(bool aVisible);
   bool isPausePressed();

protected:
   void on_btn_play_clicked();
   void on_btn_stop_clicked();
   void on_btn_pause_clicked();
   void on_btn_pa_clicked();
   void on_btn_live_ch_clicked();
   void on_btn_gps_clicked();
   void on_btn_settings_clicked();

   void on_btn_route_clicked();
   void on_btn_prev_stop_clicked();
   void on_btn_next_stop_clicked();
   void on_btn_prev_eloc_clicked();
   void on_btn_next_eloc_clicked();

   void on_settings_win_closed();

   bool on_switch_log_debug_changed(GdkEventTouch* aEvtTouch);

private:
   void initButtonsAndImgs();

   void showStops();
   void showElocutionsAndNextStop(unsigned int aNumElocs, unsigned int aFirstRow);

   const char* secondsToTime(int aSeconds, int aTotalSeconds);

   void clearAll();

   Gpio                        mGpio;
   Gps                         mGPS;
   Sound                       mSound;
   Glib::RefPtr<Gtk::Builder>  mBuilder;

   // Pointers to the labels that will represent the route-stops-comments
   Gtk::Label* mpLabelRow[NUM_ROWS];

   Gtk::Label* mpLabelPlayTime;
   Gtk::LevelBar* mpProgressBar;

   Gtk::Button *mpBtnPlay;
   Gtk::ToggleButton *mpBtnPause, *mpBtnPA, *mpBtnLiveCH, *mpBtnGps;
   Gtk::Switch *mpSwitchLogDebug;

   SettingsWindow *mpSettingsWin;

   int mCurrSecs, mNumTotalSeconds; // for showing playing/whole time

   bool mPause_pressed;
};

#endif // GTKMM_EXAMPLE_HELLOWORLD_H

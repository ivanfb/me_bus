
#ifndef LOG_H_
#define LOG_H_

#include <gtkmm/window.h>

class Log
{
public:
   enum eLogLevel
   {
      LOG_LEVEL_DEBUG,
      LOG_LEVEL_INFO
   };

public:
   // Sets a pointer to the main window. needed for showing Dialog boxes there.
   static void setMainWindow(Gtk::Window* aMainWindow)
   {
      mPtrMainWindow = aMainWindow;
   }

   static void reopenLog(bool aDebug);
   static void openLog();
   static void closeLog();

   static void info(const char* aMsg, ...);
   static void debug(const char* aMsg, ...);
   static void error(const char* aMsg, ...);

private:
   static Gtk::Window* mPtrMainWindow;

   static eLogLevel mLogLevel;
};

#endif /* LOG_H_ */

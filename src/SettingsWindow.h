
#ifndef SETTINGSWINDOW_H_
#define SETTINGSWINDOW_H_

#include <gtkmm.h>
#include <gtkmm/window.h>

#include "Sound.h"
#include "Gpio.h"

class SettingsWindow: public Gtk::Window
{
public:
   SettingsWindow(BaseObjectType* cobject,
                  const Glib::RefPtr<Gtk::Builder>& refGlade);
   virtual ~SettingsWindow();

   void setSoundRef(Sound& aSound);

   void setGpioRef(Gpio& aGpio);

private:
   void on_btn_close_clicked();
   void on_btn_prev_pa_clicked();
   void on_btn_next_pa_clicked();
   void on_btn_prev_monitor_clicked();
   void on_btn_next_monitor_clicked();
   void on_btn_more_volume_clicked();
   void on_btn_less_volume_clicked();
   void on_btn_more_general_volume_clicked();
   void on_btn_less_general_volume_clicked();

   void showNumPaChannel();
   void showNumMonitorChannel();

   Glib::RefPtr<Gtk::Builder>  mBuilder;

   Gtk::Label* mpPaChannel, *mpMonChannel;

   int mCurrentMonitorChannel;
   int mCurrentPaChannel;

   Sound* mpSound;
   Gpio* mpGpio;
};

#endif /* SETTINGSWINDOW_H_ */

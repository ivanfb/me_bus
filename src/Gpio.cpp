
#include <pthread.h>
#include <fstream>
#include <glibmm/dispatcher.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>

#include "Log.h"
#include "Gpio.h"

#define BCM2708_PERI_BASE        0x3F000000
#define GPIO_BASE                (BCM2708_PERI_BASE + 0x200000) /* GPIO controller */
#define PAGE_SIZE (4*1024)
#define BLOCK_SIZE (4*1024)

// GPIO setup macros. Always use INP_GPIO(x) before using OUT_GPIO(x) or SET_GPIO_ALT(x,y)
#define INP_GPIO(g) *(gpio+((g)/10)) &= ~(7<<(((g)%10)*3))
#define OUT_GPIO(g) *(gpio+((g)/10)) |=  (1<<(((g)%10)*3))
#define SET_GPIO_ALT(g,a) *(gpio+(((g)/10))) |= (((a)<=3?(a)+4:(a)==4?3:2)<<(((g)%10)*3))

#define GPIO_SET *(gpio+7)  // sets   bits which are 1 ignores bits which are 0
#define GPIO_CLR *(gpio+10) // clears bits which are 1 ignores bits which are 0

#define GET_GPIO(g) (*(gpio+13)&(1<<g)) // 0 if LOW, (1<<g) if HIGH

#define GPIO_PULL *(gpio+37) // Pull up/pull down
#define GPIO_PULLCLK0 *(gpio+38) // Pull up/pull down clock

int  mem_fd;
void *gpio_map;

// I/O access
volatile unsigned *gpio;

pthread_t mThGpio; // thread that polls pedal status (BCM19)
Sound* mpSoundGpio;

// the pedal can also switch the PA status if jumper (BCM6) is '0'.
Glib::Dispatcher mPA_dispatcher;

// Set up a memory regions to access GPIO
void Gpio::setup_io()
{
   /* open /dev/mem */
   if ((mem_fd = open("/dev/mem", O_RDWR|O_SYNC) ) < 0) {
      printf("can't open /dev/mem \n");
      exit(-1);
   }

   /* mmap GPIO */
   gpio_map = mmap(
      NULL,             //Any adddress in our space will do
      BLOCK_SIZE,       //Map length
      PROT_READ|PROT_WRITE,// Enable reading & writting to mapped memory
      MAP_SHARED,       //Shared with other processes
      mem_fd,           //File to map
      GPIO_BASE         //Offset to GPIO peripheral
   );

   close(mem_fd); //No need to keep mem_fd open after mmap

   if (gpio_map == MAP_FAILED) {
      printf("mmap error %d\n", (int)gpio_map);//errno also set!
      exit(-1);
   }

   // Always use volatile pointer!
   gpio = (volatile unsigned *)gpio_map;

} // setup_io

void* checkPedalStatus(void*)
{
   bool lPedalPressed = false, lPedalPressedPrev = false;

   if (mpSoundGpio != nullptr)
   {
      while (true)
      {
         // getting pedal status
         lPedalPressed = (GET_GPIO(19) == 0); // pedal pressed when '0'

         if (lPedalPressed != lPedalPressedPrev)
         {
            if (lPedalPressed)
            {
               // getting gpio6 value (jumper)
               if (GET_GPIO(6) != 0) // jumper is '1'. The pedal plays/stops.
               {
                  if (mpSoundGpio->getStatus() != Sound::PLAYING) // not playing
                  {
                     mpSoundGpio->playFromPedal();
                     while(mpSoundGpio->getStatus() != Sound::PLAYING)
                     {
                        usleep(200000); // wait till it's actually playing
                     }
                  }
                  else if (mpSoundGpio->getStatus() == Sound::PLAYING) // playing
                  {
                     mpSoundGpio->stopFromPedal();
                     while(mpSoundGpio->getStatus() == Sound::PLAYING)
                     {
                        usleep(200000); // wait until it get's fully stopped
                     }
                  }
               }
               else // jumper is '0'
               {
                  mPA_dispatcher.emit(); // this is equivalent to push the "PA" button
               }

               usleep(500000); // delay to avoid possible fluctuations when reading GPIO_19 input
            } // if (lPedalPressed)

            lPedalPressedPrev = lPedalPressed;
         }
         usleep(100000);
      }
   }

   pthread_exit(nullptr);

   return nullptr;
}

void Gpio::start(Sound& aSound)
{
   mpSoundGpio = &aSound;

   setup_io();

   setPA_active(false);
   setPA_channel(0);
   setLiveCH_active(false);

   int rc = pthread_create(&mThGpio, nullptr, checkPedalStatus, nullptr);
   if (rc)
   {
      Log::error("Gpio: ERROR return code from pthread_create() is %d\n", rc);
   }
}

void Gpio::setPA_active(bool aActive)
{
   Log::info("Gpio: sepPA_active [%s]", aActive ? "YES" : "NO");
   sendCmd(13, aActive); // BCM13
   mPA_active = aActive;
}

void Gpio::setLiveCH_active(bool aActive)
{
   Log::info("Gpio: setLiveCH_active [%s]", aActive ? "YES" : "NO");
   sendCmd(12, aActive); // BCM12
   mLiveCH_active = aActive;
}

void Gpio::setPA_channel(char aPA_channel)
{
   Log::info("Gpio: setPA_channel [%d] (first channel is '0')", aPA_channel);
   sendCmd(17, (aPA_channel & 0x08) == 0x08); // BCM17
   sendCmd(16, (aPA_channel & 0x04) == 0x04); // BCM16
   sendCmd(15, (aPA_channel & 0x02) == 0x02); // BCM15
   sendCmd(14, (aPA_channel & 0x01) == 0x01); // BCM14
}

void Gpio::sendCmd(unsigned int aNumGpio, bool aActive)
{
   Log::info("Gpio: sending command");

   INP_GPIO(aNumGpio); // must use INP_GPIO before we can use OUT_GPIO
   OUT_GPIO(aNumGpio);

   if (aActive)
   {
      GPIO_SET = 1 << aNumGpio;
   }
   else
   {
      GPIO_CLR = 1 << aNumGpio;
   }
}

Glib::Dispatcher& Gpio::getPA_dispatcher()
{
   return mPA_dispatcher;
}


#include <sstream>
#include <iomanip>

#include <gtkmm/treeview.h>

#include "StartupWindow.h"
#include "MainWindow.h"
#include "Log.h"
#include "Model.h"
#include "Ctes.h"

MainWindow::MainWindow(BaseObjectType* cobject,
      const Glib::RefPtr<Gtk::Builder>& refGlade) :
      Gtk::Window(cobject), mBuilder(refGlade),
      mpBtnPlay(nullptr),
      mpBtnPause(nullptr),
      mpBtnPA(nullptr),
      mpBtnLiveCH(nullptr),
      mpBtnGps(nullptr),
      mpSettingsWin(nullptr),
      mCurrSecs(0),
      mNumTotalSeconds(0),
      mPause_pressed(false)
{
   Log::setMainWindow(this);

   set_size_request(800, 480);

   override_background_color(Gdk::RGBA("#ffffff"));

   set_decorated(false); // quit the typical close, minimize buttons.

   mSound.setMainwindow(this);
   mGPS.setSoundRef(mSound);

   Gtk::Label* lpLabelVersion = nullptr;
   mBuilder->get_widget("labelVersion", lpLabelVersion);
   if (lpLabelVersion)
   {
      lpLabelVersion->set_text(Ctes::VERSION);
   }

   // get a reference to each label that will let show the route-stops-comments
   mBuilder->get_widget("labelRow0", mpLabelRow[0]);
   mBuilder->get_widget("labelRow1", mpLabelRow[1]);
   mBuilder->get_widget("labelRow2", mpLabelRow[2]);
   mBuilder->get_widget("labelRow3", mpLabelRow[3]);
   mBuilder->get_widget("labelRow4", mpLabelRow[4]);
   mBuilder->get_widget("labelRow5", mpLabelRow[5]);
   mBuilder->get_widget("labelRow6", mpLabelRow[6]);
   mBuilder->get_widget("labelRow7", mpLabelRow[7]);
   mBuilder->get_widget("labelRow8", mpLabelRow[8]);

   if (mpLabelRow[0] == nullptr || mpLabelRow[1] == nullptr
         || mpLabelRow[2] == nullptr || mpLabelRow[3] == nullptr
         || mpLabelRow[4] == nullptr || mpLabelRow[5] == nullptr
         || mpLabelRow[6] == nullptr || mpLabelRow[7] == nullptr
         || mpLabelRow[8] == nullptr)
   {
      Log::error("Mainwindow error: couldn't get a reference to label");
      return;
   }

   mBuilder->get_widget("lblTime", mpLabelPlayTime);

   mBuilder->get_widget("playProgressBar", mpProgressBar);
   mpProgressBar->set_min_value(0);

   show_all();

   mpLabelPlayTime->override_color(Ctes::COLOR_WHITE);
   mpProgressBar->hide();

   mGpio.getPA_dispatcher().connect(sigc::mem_fun(*this, &MainWindow::on_btn_pa_clicked));
}

MainWindow::~MainWindow()
{
   if (mpSettingsWin != nullptr)
   {
      delete mpSettingsWin;
   }
}

void MainWindow::init()
{
   Model::parse(); // load the data model from json

   initButtonsAndImgs();
   showRoutes();

   mGpio.start(mSound);

   mGPS.start();
   mSound.playMusicInAllChannels();

   StartupWindow *mpStartupWin = nullptr;
   mBuilder->get_widget_derived("winMeLogo", mpStartupWin);
   if (mpStartupWin != nullptr)
   {
      mpStartupWin->show();
   }
}

void MainWindow::initButtonsAndImgs()
{
   Gtk::Button* lpButton = nullptr;
   mBuilder->get_widget("btnPlay", mpBtnPlay);
   if (mpBtnPlay)
   {
      mpBtnPlay->signal_clicked().connect(
            sigc::mem_fun(*this, &MainWindow::on_btn_play_clicked));

      mpBtnPlay->set_image(*Ctes::mpImgPlayByGps);
      mpBtnPlay->set_always_show_image(false);
   }

   mBuilder->get_widget("btnStop", lpButton);
   if (lpButton)
   {
      lpButton->signal_clicked().connect(
            sigc::mem_fun(*this, &MainWindow::on_btn_stop_clicked));
   }

   mBuilder->get_widget("btnPause", mpBtnPause);
   if (mpBtnPause)
   {
      mpBtnPause->signal_clicked().connect(
            sigc::mem_fun(*this, &MainWindow::on_btn_pause_clicked));
      mpBtnPause->set_image(*Ctes::mpImgGrayPauseBtn);
      mpBtnPause->set_always_show_image(true);
   }

   mBuilder->get_widget("btnPA", mpBtnPA);
   if (mpBtnPA)
   {
      mpBtnPA->signal_toggled().connect(
            sigc::mem_fun(*this, &MainWindow::on_btn_pa_clicked));
      mpBtnPA->set_image(*Ctes::mpImgGrayCircle_PA);
      mpBtnPA->set_always_show_image(true);
   }

   mBuilder->get_widget("btnLiveCH", mpBtnLiveCH);
   if (mpBtnLiveCH)
   {
      mpBtnLiveCH->signal_toggled().connect(
            sigc::mem_fun(*this, &MainWindow::on_btn_live_ch_clicked));
      mpBtnLiveCH->set_image(*Ctes::mpImgGrayCircle_LiveCH);
      mpBtnLiveCH->set_always_show_image(true);
   }

   mBuilder->get_widget("btnGPS", mpBtnGps);
   if (mpBtnGps)
   {
      mpBtnGps->signal_toggled().connect(
            sigc::mem_fun(*this, &MainWindow::on_btn_gps_clicked));
      mpBtnGps->set_image(*Ctes::mpImgGrayCircle_GPS);
      mpBtnGps->set_always_show_image(true);
   }

   mBuilder->get_widget("btnSettings", lpButton);
   if (lpButton)
   {
      lpButton->signal_clicked().connect(
            sigc::mem_fun(*this, &MainWindow::on_btn_settings_clicked));
      lpButton->set_image(*Ctes::mpImgSettings);
      lpButton->set_always_show_image(true);
   }

   mBuilder->get_widget("btnRoute", lpButton);
   if (lpButton)
   {
      lpButton->signal_clicked().connect(
            sigc::mem_fun(*this, &MainWindow::on_btn_route_clicked));
      lpButton->set_image(*Ctes::mpImgRoute);
      lpButton->set_always_show_image(true);
   }

   mBuilder->get_widget("btnPrevStop", lpButton);
   if (lpButton)
   {
      lpButton->signal_clicked().connect(
            sigc::mem_fun(*this, &MainWindow::on_btn_prev_stop_clicked));
      lpButton->set_image(*Ctes::mpImgPrevStop);
      lpButton->set_always_show_image(true);
   }

   mBuilder->get_widget("btnNextStop", lpButton);
   if (lpButton)
   {
      lpButton->signal_clicked().connect(
            sigc::mem_fun(*this, &MainWindow::on_btn_next_stop_clicked));
      lpButton->set_image(*Ctes::mpImgNextStop);
      lpButton->set_always_show_image(true);
   }

   mBuilder->get_widget("btnPrevComment", lpButton);
   if (lpButton)
   {
      lpButton->signal_clicked().connect(
            sigc::mem_fun(*this, &MainWindow::on_btn_prev_eloc_clicked));
      lpButton->set_image(*Ctes::mpImgPrevComment);
      lpButton->set_always_show_image(true);
   }

   mBuilder->get_widget("btnNextComment", lpButton);
   if (lpButton)
   {
      lpButton->signal_clicked().connect(
            sigc::mem_fun(*this, &MainWindow::on_btn_next_eloc_clicked));
      lpButton->set_image(*Ctes::mpImgNextComment);
      lpButton->set_always_show_image(true);
   }

   mpSwitchLogDebug = nullptr;
   mBuilder->get_widget("switchLogDebug", mpSwitchLogDebug);
   if (mpSwitchLogDebug)
   {
      mpSwitchLogDebug->signal_touch_event().connect(
            sigc::mem_fun(*this, &MainWindow::on_switch_log_debug_changed));
   }
}

void MainWindow::on_btn_play_clicked()
{
   if (mSound.getStatus() == Sound::PLAYING_MUSIC)
   {  // Only play the commentaries if it's playing music.
      // when all the musics end then the commentaries play will start.
      Log::info("MainWindow::on_btn_play_clicked");
      mSound.play();
   }
}

void MainWindow::on_btn_pa_clicked()
{
   if (mGpio.isPA_active())
   {
      mpBtnPA->set_image(*Ctes::mpImgGrayCircle_PA);
      mGpio.setPA_active(false);
   }
   else
   {
      mpBtnPA->set_image(*Ctes::mpImgRedCircle_PA);
      mGpio.setPA_active(true);
   }
   Log::info("MainWindow::on_btn_pa_clicked isPaActive[%s]",
         mGpio.isPA_active() ? "TRUE":"FALSE");
}

void MainWindow::on_btn_live_ch_clicked()
{
   if (mGpio.isLiveCH_active())
   {
      mpBtnLiveCH->set_image(*Ctes::mpImgGrayCircle_LiveCH);
      mGpio.setLiveCH_active(false);
   }
   else
   {
      mpBtnLiveCH->set_image(*Ctes::mpImgRedCircle_LiveCH);
      mGpio.setLiveCH_active(true);
   }

   Log::info("MainWindow::on_btn_live_ch_clicked isLiveChActive[%s]",
         mGpio.isLiveCH_active() ? "TRUE":"FALSE");
}

void MainWindow::on_btn_gps_clicked()
{
   static bool lGPS_active = true;

   if (lGPS_active)
   {
      lGPS_active = false;
      mpBtnGps->set_image(*Ctes::mpImgRedCircle_GPS);
   }
   else
   {
      lGPS_active = true;
      mpBtnGps->set_image(*Ctes::mpImgGrayCircle_GPS);
   }

   mGPS.setActive(lGPS_active);
   Log::info("MainWindow::on_btn_gps_clicked isGpsActive[%s]",
             lGPS_active ? "TRUE":"FALSE");
}

void MainWindow::on_btn_stop_clicked()
{
   if (mSound.getStatus() != Sound::PLAYING_MUSIC)
   {
      Log::info("MainWindow::on_btn_stop_clicked");
      mSound.stop();
      Model::goNextElocInRoute();
      showRoutes();
   }
}

void MainWindow::unchekPauseButton()
{ // put the button on it's initial status view
   if (mPause_pressed)
   {
      Log::info("MainWindow::unchekPauseButton");
      mpBtnPause->clicked();
   }
}

void MainWindow::on_btn_pause_clicked()
{
   if (mPause_pressed)
   {
      mPause_pressed = false;
      mpBtnPause->set_image(*Ctes::mpImgGrayPauseBtn);
   }
   else
   {
      mPause_pressed = true;
      mpBtnPause->set_image(*Ctes::mpImgRedPauseBtn);
   }
   Log::info("MainWindow::on_btn_pause_clicked isPausePressed[%s]",
             mPause_pressed ? "TRUE":"FALSE");
}

bool MainWindow::isPausePressed()
{
   return mPause_pressed;
}

void MainWindow::on_btn_settings_clicked()
{
   mBuilder->get_widget_derived("winSettings", mpSettingsWin);
   if (mpSettingsWin != nullptr)
   {
      Log::info("MainWindow::on_btn_settings_clicked");
      mpSettingsWin->setSoundRef(mSound);
      mpSettingsWin->setGpioRef(mGpio);
      mpSettingsWin->signal_hide().connect(
            sigc::mem_fun(*this, &MainWindow::on_settings_win_closed));
      mpSettingsWin->show();
   }
}

void MainWindow::on_settings_win_closed()
{
   Log::info("MainWindow::on_settings_win_closed");
   mpSettingsWin = nullptr;
}

void MainWindow::on_btn_route_clicked()
{
   Log::info("MainWindow::on_btn_route_clicked");
   Model::goNextRoute();
   showRoutes();
}

void MainWindow::on_btn_prev_stop_clicked()
{
   Log::info("MainWindow::on_btn_prev_stop_clicked");
   Model::goPrevStop();
   showRoutes();
}

void MainWindow::on_btn_next_stop_clicked()
{
   Log::info("MainWindow::on_btn_next_stop_clicked");
   Model::goNextStop();
   showRoutes();
}

void MainWindow::on_btn_prev_eloc_clicked()
{
   Log::info("MainWindow::on_btn_prev_eloc_clicked");
   Model::goPrevEloc();
   showRoutes();
}

void MainWindow::on_btn_next_eloc_clicked()
{
   Log::info("MainWindow::on_btn_next_eloc_clicked");
   Model::goNextEloc();
   showRoutes();
}

void MainWindow::showRoutes()
{
   Log::debug("MainWindow::showRoutes");
   clearAll();

   if (Model::getRoutes().size() == 0)
   {
      Log::error("MainWindow error: model is empty");
      return;
   }

   mpLabelRow[0]->set_text("  " + Model::getCurrentRoute()->mRouteName);
   mpLabelRow[0]->override_background_color(Ctes::BACK_COLOR_CURRENT_ROUTE);
   mpLabelRow[0]->override_color(Ctes::COLOR_WHITE);

   showStops();
}

void MainWindow::showStops()
{
   Log::debug("MainWindow::showStops");
   // The most usual case is when there are more or equal than 3 stops
   if (Model::getCurrentRoute()->mStops.size() >= 3)
   {
      // Prev stop
      const Model::Stop* lPrevStop = Model::getPrevStop();

      // add the required space for always have 2 digits
      std::stringstream lNum;
      lNum << std::setw(2) << std::setfill('0') << lPrevStop->mNumStop;

      mpLabelRow[1]->set_text(Ctes::INDENT_STOPS + lNum.str() + " " + lPrevStop->mStopName);

      // Current Stop
      const Model::Stop* lCurrStop = Model::getCurrentStop();
      lNum.str(std::string()); // clear the content
      lNum << std::setw(2) << std::setfill('0') << lCurrStop->mNumStop;
      mpLabelRow[2]->set_text(Ctes::INDENT_STOPS + lNum.str() + " " + lCurrStop->mStopName);
      mpLabelRow[2]->override_background_color(Ctes::BACK_COLOR_CURRENT_STOP);
      mpLabelRow[2]->override_color(Ctes::COLOR_WHITE);

      showElocutionsAndNextStop(lCurrStop->mElocs.size(), 3 /* first row */);
   }
   else // there are less than 3 stops (0, 1, or 2)
   {
      // the current stop is shown in the first row
      const Model::Stop* lCurrStop = Model::getCurrentStop();
      std::stringstream lNum;
      lNum << std::setw(2) << std::setfill('0') << lCurrStop->mNumStop;

      mpLabelRow[1]->set_text(Ctes::INDENT_STOPS + lNum.str() + " " + lCurrStop->mStopName);
      mpLabelRow[1]->override_background_color(Ctes::BACK_COLOR_CURRENT_STOP);
      mpLabelRow[1]->override_color(Ctes::COLOR_WHITE);

      showElocutionsAndNextStop(lCurrStop->mElocs.size(), 2 /* first row */);
   }
}

void MainWindow::showElocutionsAndNextStop(unsigned int aNumElocs, unsigned int aFirstRow)
{
   Log::debug("MainWindow::showElocutionsAndNextStop");
   std::stringstream lNum;

   if (aNumElocs >= 5)
   { // show the comments in circular way

      // prev prev comment
      const Model::Elocution* lPrevPrevEloc = Model::getPrevNextEloc(Model::PREV, Model::TWO);
      lNum.str(std::string()); // clear the content
      lNum << std::setw(2) << std::setfill('0') << lPrevPrevEloc->mNumEloc;
      mpLabelRow[aFirstRow]->set_text(Ctes::INDENT_ELOCS + lNum.str() + " " + lPrevPrevEloc->mElocutionName);

      // prev comment
      const Model::Elocution* lPrevEloc = Model::getPrevNextEloc(Model::PREV, Model::ONE);
      lNum.str(std::string()); // clear the content
      lNum << std::setw(2) << std::setfill('0') << lPrevEloc->mNumEloc;
      mpLabelRow[aFirstRow + 1]->set_text(Ctes::INDENT_ELOCS + lNum.str() + " " + lPrevEloc->mElocutionName);

      // current comment
      const Model::Elocution* lCurrEloc = Model::getCurrentEloc();
      lNum.str(std::string()); // clear the content
      lNum << std::setw(2) << std::setfill('0') << lCurrEloc->mNumEloc;
      mpLabelRow[aFirstRow + 2]->set_text(Ctes::INDENT_ELOCS + lNum.str() + " " + lCurrEloc->mElocutionName);
      mpLabelRow[aFirstRow + 2]->override_background_color(Ctes::BACK_COLOR_CURRENT_ELOC);
      mpLabelRow[aFirstRow + 2]->override_color(Ctes::COLOR_WHITE);

      // next comment
      const Model::Elocution* lNextEloc = Model::getPrevNextEloc(Model::NEXT, Model::ONE);
      lNum.str(std::string()); // clear the content
      lNum << std::setw(2) << std::setfill('0') << lNextEloc->mNumEloc;
      mpLabelRow[aFirstRow + 3]->set_text(Ctes::INDENT_ELOCS + lNum.str() + " " + lNextEloc->mElocutionName);

      // next next comment
      const Model::Elocution* lNextNextEloc = Model::getPrevNextEloc(Model::NEXT, Model::TWO);
      lNum.str(std::string()); // clear the content
      lNum << std::setw(2) << std::setfill('0') << lNextNextEloc->mNumEloc;
      mpLabelRow[aFirstRow + 4]->set_text(Ctes::INDENT_ELOCS + lNum.str() + " " + lNextNextEloc->mElocutionName);

      if (Model::getCurrentRoute()->mStops.size() > 1)
      {
         // next stop
         const Model::Stop* lNextStop = Model::getNextStop();
         lNum.str(std::string()); // clear the content
         lNum << std::setw(2) << std::setfill('0') << lNextStop->mNumStop;
         mpLabelRow[aFirstRow + 5]->set_text(Ctes::INDENT_STOPS + lNum.str() + " " + lNextStop->mStopName);
      }
   }
   else // num elocs < 5
   {
      int i = aFirstRow; // start from row 2.
      for (Model::Elocution* lEloc : Model::getCurrentElocsList())
      {
         if (lEloc->isCurrent())
         {
            mpLabelRow[i]->override_background_color(Ctes::BACK_COLOR_CURRENT_ELOC);
            mpLabelRow[i]->override_color(Ctes::COLOR_WHITE);
         }

         lNum.str(std::string()); // clear the content
         lNum << std::setw(2) << std::setfill('0') << lEloc->mNumEloc;
         mpLabelRow[i++]->set_text(Ctes::INDENT_ELOCS + lNum.str() + " " + lEloc->mElocutionName);
      }

      if (Model::getCurrentRoute()->mStops.size() > 1)
      {
         // next stop
         const Model::Stop* lNextStop = Model::getNextStop();
         lNum.str(std::string()); // clear the content
         lNum << std::setw(2) << std::setfill('0') << lNextStop->mNumStop;
         mpLabelRow[i]->set_text(Ctes::INDENT_STOPS + lNum.str() + " " + lNextStop->mStopName);
      }
   }
}

void MainWindow::clearAll()
{
   for (int i = 0; i < NUM_ROWS; i++)
   {
      mpLabelRow[i]->override_background_color(Gdk::RGBA("#ffffff"));
      mpLabelRow[i]->override_color(Gdk::RGBA("#000000"));
      mpLabelRow[i]->set_text(std::string());
   }
}

// Starts the timer that will update the progress bar and progress time.
// aNumSeconds - represents the duration of the longest elocution.
void MainWindow::startPlayProgress(int aNumSeconds)
{
   mCurrSecs = 0;
   mNumTotalSeconds = aNumSeconds;

   mpLabelPlayTime->override_color(Ctes::COLOR_BLACK); // to show the item without any collateral scalation
   mpLabelPlayTime->set_text(secondsToTime(mCurrSecs, mNumTotalSeconds));

   mpProgressBar->show();
   mpProgressBar->set_value(mCurrSecs);
   mpProgressBar->set_max_value(mNumTotalSeconds);
}

void MainWindow::updateProgress()
{
   mCurrSecs++;
   if (mCurrSecs <= mNumTotalSeconds)
   {
      mpLabelPlayTime->set_text(secondsToTime(mCurrSecs, mNumTotalSeconds));
      mpProgressBar->set_value(mCurrSecs);
   }
}

void MainWindow::stopPlayProgress()
{
   Log::debug("MainWindow: stopPlayProgress");
   mpLabelPlayTime->override_color(Ctes::COLOR_WHITE);
   mpProgressBar->hide();
}

const char* MainWindow::secondsToTime(int aSeconds, int aTotalSeconds)

{
   std::ostringstream oss;

   oss << std::setfill('0') << std::setw(2) << int(aSeconds / 60) << ':';
   oss << std::setw(2) << int(aSeconds % 60);

   oss << " / ";

   oss << std::setfill('0') << std::setw(2) << int(aTotalSeconds / 60) << ':';
   oss << std::setw(2) << int(aTotalSeconds % 60);

   return oss.str().c_str();
}

void MainWindow::setGpsIconVisible(bool aVisible)
{
   if (mpBtnPlay != nullptr)
   {
      mpBtnPlay->set_image(*Ctes::mpImgPlayByGps);
      mpBtnPlay->set_always_show_image(aVisible);
   }
}

void MainWindow::setPedalIconVisible(bool aVisible)
{
   if (mpBtnPlay != nullptr)
   {
      mpBtnPlay->set_image(*Ctes::mpImgPlayByPedal);
      mpBtnPlay->set_always_show_image(aVisible);
   }
}

bool MainWindow::on_switch_log_debug_changed(GdkEventTouch* aEvtTouch)
{
   if (aEvtTouch->type == GDK_TOUCH_END)
   {
      bool lEnableDebug = mpSwitchLogDebug->get_active();
      // inverse logic. this method is called right before the switch state has changed.
      Log::reopenLog(!lEnableDebug);
   }

   return false;
}

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <iostream>
#include <fstream>
#include <set>
#include <string>

#include <cstring>
#include <cstdlib>

#include "Model.h"
#include "Ctes.h"
#include "Log.h"

// Short alias for this namespace
namespace pt = boost::property_tree;

int Model::mMarginGpsElocs = 0;

Model::VectorElocs Model::mAllElocs;
Model::VectorRoutes Model::mRoutes;
Model::BookMark Model::mBookMark(0, 0, 0);

Model::Route* Model::mpNullRoute = new Route("Route null", 0);
Model::Stop* Model::mpNullStop = new Stop("Stop null", 1 /*stop index*/,
      nullptr /*parent*/);
Model::Elocution* Model::mpNullEloc = new Elocution("Commentary null",
      0 /* eloc index*/, 0 /*abs eloc index*/, nullptr /*parent*/,
      false /*autoplay*/);

inline bool fileExists (const std::string& name) {
  struct stat buffer;
  return (stat (name.c_str(), &buffer) == 0);
}

Model::~Model()
{
   for (Route* r : mRoutes)
   {
      delete (r);
   }

   mRoutes.clear();
}

void Model::parse()
{
   std::set<std::string> lMissingFiles;

   try
   {
      unsigned int lAbsoluteElocIndex = 0; // for gps plays purposes

      pt::ptree lDoc;
      pt::read_json(Ctes::JSON_FILE, lDoc);

      pt::ptree route;

      int lRouteIndex = 0;
      for (pt::ptree::value_type &lRoute : lDoc.get_child(Ctes::ROUTES))
      {
         std::string lRouteName = lRoute.second.get<std::string>(Ctes::ROUTE_NAME);

         Route* lpRoute = new Route(lRouteName, lRouteIndex++);

         // Stops
         int lStopIndex = 0;
         for (pt::ptree::value_type &lStop : lRoute.second.get_child(Ctes::STOPS))
         {
            std::string lStopName = lStop.second.get<std::string>(Ctes::STOP_NAME);

            Stop* lpStop = new Stop(lStopName, lStopIndex++, lpRoute);

            // Musics
            for (pt::ptree::value_type &lMusic : lStop.second.get_child(Ctes::MUSICS))
            {
               std::string lFile = lMusic.second.data();

               if ( ! fileExists(lFile.c_str()))
               {
                  lMissingFiles.insert(lFile);
               }

               lpStop->mMusics.push_back(lFile);
            }

            // Elocutions
            int lElocIndex = 0;
            for (pt::ptree::value_type &lEloc : lStop.second.get_child(Ctes::ELOCUTIONS))
            {
               std::string lElocName = lEloc.second.get<std::string>(Ctes::ELOC_NAME);

               bool lIsAutoplay = lEloc.second.get<bool>(Ctes::ELOC_AUTOPLAY);

               Elocution* lpEloc = new Elocution(lElocName, lElocIndex++,
                                                 lAbsoluteElocIndex++,
                                                 lpStop,
                                                 lIsAutoplay);
               mAllElocs.push_back(lpEloc);

               // Elocution files
               for (pt::ptree::value_type &lElocFile : lEloc.second.get_child(Ctes::ELOC_FILES))
               {
                  std::string lFile = lElocFile.second.data();

                  if ( ! fileExists(lFile.c_str()))
                  {
                     lMissingFiles.insert(lFile);
                  }

                  lpEloc->mAudioFiles.push_back(lFile);
               }

               lpStop->mElocs.push_back(lpEloc);
            }

            lpRoute->mStops.push_back(lpStop);
         }

         mRoutes.push_back(lpRoute);
      }
   }
   catch (std::exception& e)
   {
      Log::error("Model: exception [%s]", e.what());
   }

   if ( ! lMissingFiles.empty())
   {
      Log::error("Missing files in model. Writing missing files in log file.");
      for (const std::string &lFileName : lMissingFiles)
      {
         Log::info(lFileName.c_str());
      }
   }

   parseGpsInfo();
}

void Model::midaSegonsGPS(double latitud, double *metres_lat, double *metres_lon)
{
   double m1, m2, m3, m4, p1, p2, p3;

   // Convert latitude to radians
   double lat = latitud * double ((2.0 * 3.141592654) / 360.0);

   // Set up "Constants"
   m1 = 111132.92;      // latitude calculation term 1
   m2 = -559.82;     // latitude calculation term 2
   m3 = 1.175;       // latitude calculation term 3
   m4 = -0.0023;     // latitude calculation term 4
   p1 = 111412.84;      // longitude calculation term 1
   p2 = -93.5;       // longitude calculation term 2
   p3 = 0.118;       // longitude calculation term 3

   // Calculate the length of a degree of latitude and longitude in meters
   *metres_lat = m1 + (m2 * cos(2 * lat)) + (m3 * cos(4 * lat))
         + (m4 * cos(6 * lat));
   *metres_lon = (p1 * cos(lat)) + (p2 * cos(3 * lat)) + (p3 * cos(5 * lat));

   //passem a metres segon
   *metres_lat /= 3600;
   *metres_lon /= 3600;
}

template<typename Out>
void split(const std::string &s, char delim, Out result) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        *(result++) = item;
    }
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, std::back_inserter(elems));
    return elems;
}

void Model::parseGpsInfo()
{
   // Meters that represents one second in latitude and longitude
   // The longitude ones vary when position is close to poles.
   // The latitude ones keep practically unchanged.
   double METRES_SEG_LAT;
   double METRES_SEG_LON;

   std::string lLine;
   std::ifstream lGpsfile(Ctes::GPS_FILE);

   if (!fileExists(Ctes::GPS_FILE))
   {
      Log::error("File %s does not exist", Ctes::GPS_FILE);
      return;
   }

   if (lGpsfile.is_open())
   {
      // getting the GPS margin
      if (std::getline(lGpsfile, lLine))
      {
         // elocution margin. Maximum allowed number of elocutions to get skipped.
         mMarginGpsElocs = atoi(lLine.c_str());

         bool lFirstItem = true; // used to calibrate with just first position

         // for debug purposes. Keeps track of posgps.inf route-stop-eloc not present in json model
         std::set<std::string> lMissingRSE;
         char lMsg[256];

         while (std::getline(lGpsfile, lLine))
         {
            if (lLine.find('#') != std::string::npos)
            {
               break;
            }
            // parsing the line
            // example: 1,1,3,25,90,170,40,24,54.5849,N,3,41,31.8,W
            std::vector<std::string> v = split(lLine.c_str(), ',');
            if (v.size() > 1)
            {
//          Route == v[0]         Stop == v[1]       Eloc == v[2]
//          MarginLatLon == v[3]  Direction == v[4]  MarginDir == v[5]
//          LatHour == v[6]       LatMin == v[7]     LatSecs == v[8]    NS == v[9]
//          LonHours == v[10]     LonMin == v[11]    LonSecs == v[12]   WE == v[13]

               int lRoute = atoi(v[0].c_str()); // route obtained from posgps.inf starts at '1'
               int lStop = atoi(v[1].c_str());
               int lEloc = atoi(v[2].c_str());

               double lLat = atof(v[6].c_str()) * 3600 + atof(v[7].c_str()) * 60 + atof(v[8].c_str());
               double lLon = atof(v[10].c_str()) * 3600 + atof(v[11].c_str()) * 60 + atof(v[12].c_str());

               if (lFirstItem)
               { // calibrate GPS. Lat is passed in degrees.
                  midaSegonsGPS(lLat / 3600, &METRES_SEG_LAT, &METRES_SEG_LON);
                  lFirstItem = false; // don't enter here again
               }

               double lMargeLat = atof(v[3].c_str()) / METRES_SEG_LAT;
               double lMargeLon = atof(v[3].c_str()) / METRES_SEG_LON;

               double lDir = atof(v[4].c_str());
               double lMarginDir = atof(v[5].c_str());

               char lNS = v[9][0]; // getting the first char of the std::string
               char lWE = v[13][0]; // getting the first char of the std::string

               // check if the route, stop, eloc is present in 'posgps.ing' file
               // matches with the data model previously loaded from JSON file.
               if (!checkExists(lRoute - 1, lStop - 1, lEloc - 1))
               {
                  snprintf(lMsg, 256, "Model: Item not present in JSON file: Route [%u] Stop [%u] Eloc [%u]",
                           lRoute, lStop, lEloc);
                  lMissingRSE.insert(lMsg);
                  continue; // go for next line
               }
               else // the route-stop-eloc exists within the current JSON data model
               {
                  Elocution* lElocGps = mRoutes[lRoute - 1]->mStops[lStop - 1]->mElocs[lEloc - 1];

                  lElocGps->mpGpsInfo = new LocucioGPS(lMargeLat, lMargeLon, lDir, lMarginDir,
                                                       lLat, lLon, lNS, lWE);
               }
            }
         }

         if (!lMissingRSE.empty())
         {
            Log::error("There are some posgps items that are not present in json model. Writing them in log file.");
            for (const std::string &lMissingItem : lMissingRSE)
            {
               Log::info(lMissingItem.c_str());
            }
         }
      }

      lGpsfile.close();
   }
}

bool Model::checkExists(unsigned int aRoute,
                        unsigned int aStop, unsigned int aEloc)
{
   if (aRoute < mRoutes.size())
   {
      if (aStop < mRoutes[aRoute]->mStops.size())
      {
         if (aEloc < mRoutes[aRoute]->mStops[aStop]->mElocs.size())
         {
            return true;
         }
      }
   }

   return false;
}

// Returns a pointer to the current playing route.
const Model::Route* Model::getCurrentRoute()
{
   if (mBookMark.mRoute >= Model::getRoutes().size())
   {
      Log::error("Mainwindow error: current route is beyond the limit [%u]",
            mBookMark.mRoute);
      return mpNullRoute;
   }

   return mRoutes[mBookMark.mRoute];
}

// Returns a pointer to the previous stop given the current route-stop
const Model::Stop* Model::getPrevStop()
{
   const Route* lCurrRou = getCurrentRoute();
   if (mBookMark.mStop >= lCurrRou->mStops.size())
   {
      return mpNullStop;
   }

   if (mBookMark.mStop == 0)
   {
      return lCurrRou->mStops[lCurrRou->mStops.size() - 1];
   }

   return lCurrRou->mStops[mBookMark.mStop - 1];
}

const Model::Stop* Model::getCurrentStop()
{
   const Route* lCurrRou = getCurrentRoute();
   if (mBookMark.mStop >= lCurrRou->mStops.size())
   {
      Log::error("Mainwindow error: current stop is beyond the limit [%u] "
            "for route [%u]", mBookMark.mStop, mBookMark.mRoute);
      return mpNullStop;
   }

   return lCurrRou->mStops[mBookMark.mStop];
}

const Model::Stop* Model::getNextStop()
{
   const Route* lCurrRou = getCurrentRoute();
   if (mBookMark.mStop >= lCurrRou->mStops.size())
   {
      return mpNullStop;
   }

   if (mBookMark.mStop == (lCurrRou->mStops.size() - 1))
   {
      return lCurrRou->mStops[0];
   }

   return lCurrRou->mStops[mBookMark.mStop + 1];
}

// aPrevNext: allows specify the direction where to look for. PREV or NEXT from
//            the current/selected elocution.
// aNumJumps: represents the number of jumps (ONE or TWO) from the
//            current/select elocution.
const Model::Elocution* Model::getPrevNextEloc(ePrevNext aPrevNext,
      eNumJumps aNumJumps)
{
   const Stop* lCurrStop = getCurrentStop();
   unsigned int lNumElocs = lCurrStop->mElocs.size();

   if (mBookMark.mEloc >= lNumElocs)
   {
      Log::error("Model::getPrevPrevEloc error");
      return mpNullEloc;
   }
   else
      if (lNumElocs < 5 && aNumJumps != ONE)
      {
         Log::error("Model::getPrevNextEloc error: at least there should be 7 "
               "comments for circular mode");
         return mpNullEloc;
      }

   if (aNumJumps == TWO)
   {
      if (aPrevNext == PREV)
      {
         if (mBookMark.mEloc == 0)
         {
            return lCurrStop->mElocs[lNumElocs - 2];
         }
         else
            if (mBookMark.mEloc == 1)
            {
               return lCurrStop->mElocs[lNumElocs - 1];
            }
         return lCurrStop->mElocs[mBookMark.mEloc - 2];
      }
      else
         if (aPrevNext == NEXT)
         {
            if (mBookMark.mEloc == (lNumElocs - 1)) // last eloc
            {
               return lCurrStop->mElocs[1];
            }
            else
               if (mBookMark.mEloc == (lNumElocs - 2))
               {
                  return lCurrStop->mElocs[0];
               }
            return lCurrStop->mElocs[mBookMark.mEloc + 2];
         }
   }
   else
      if (aNumJumps == ONE)
      {
         if (aPrevNext == PREV)
         {
            if (mBookMark.mEloc == 0)
            {
               return lCurrStop->mElocs[lNumElocs - 1];
            }
            return lCurrStop->mElocs[mBookMark.mEloc - 1];
         }
         else
            if (aPrevNext == NEXT)
            {
               if (mBookMark.mEloc == (lNumElocs - 1)) // last eloc
               {
                  return lCurrStop->mElocs[0];
               }
               return lCurrStop->mElocs[mBookMark.mEloc + 1];
            }
      }

   Log::error("Undefined error in Model::getPrevNextEloc");
   return mpNullEloc;
}

const Model::Elocution* Model::getCurrentEloc()
{
   const Stop* lCurrStop = getCurrentStop();
   if (mBookMark.mEloc >= lCurrStop->mElocs.size())
   {
      return mpNullEloc;
   }
   return lCurrStop->mElocs[mBookMark.mEloc];
}

const std::vector<Model::Elocution *>& Model::getCurrentElocsList()
{
   return getCurrentStop()->mElocs;
}

void Model::goNextRoute()
{
   if (mBookMark.mRoute == (mRoutes.size() - 1))
   {
      mBookMark.mRoute = 0;
   }
   else
   {
      mBookMark.mRoute++;
   }

   mBookMark.mStop = mBookMark.mEloc = 0;
}

void Model::goNextStop()
{
   if (mBookMark.mStop == (mRoutes[mBookMark.mRoute]->mStops.size() - 1))
   {
      mBookMark.mStop = 0;
   }
   else
   {
      mBookMark.mStop++;
   }

   mBookMark.mEloc = 0;
}

void Model::goPrevStop()
{
   if (mBookMark.mStop == 0)
   {
      mBookMark.mStop =
            mRoutes[mBookMark.mRoute]->mStops.size() - 1;
   }
   else {
      mBookMark.mStop--;
   }

   mBookMark.mEloc = 0;
}

void Model::goNextEloc()
{
   if (mBookMark.mEloc ==
         (mRoutes[mBookMark.mRoute]->mStops[mBookMark.mStop]->mElocs.size() - 1))
   {
      mBookMark.mEloc = 0;
   }
   else {
      mBookMark.mEloc++;
   }
}

void Model::goPrevEloc()
{
   if (mBookMark.mEloc == 0)
   {
      mBookMark.mEloc =
            mRoutes[mBookMark.mRoute]->mStops[mBookMark.mStop]->mElocs.size() - 1;
   }
   else {
      mBookMark.mEloc--;
   }
}

// This method moves the bookmark to the next stop within the same route.
void Model::goNextElocInRoute()
{
   if (mBookMark.mEloc == (getCurrentStop()->mElocs.size() - 1))
   {
      mBookMark.mEloc = 0;

      // go next stop
      if (mBookMark.mStop == getCurrentRoute()->mStops.size() - 1)
      {
         mBookMark.mStop = 0;
      }
      else
      {
         mBookMark.mStop++;
      }
   }
   else
   {
      mBookMark.mEloc++;
   }
}

void Model::setCurrentStop(int aStop)
{
   mBookMark.mStop = aStop;
   mBookMark.mEloc = 0;
}

void Model::setCurrentEloc(int aEloc)
{
   mBookMark.mEloc = aEloc;
}

// Obtains the audio file for the corresponding channel number
void Model::getCommentAudioAtChannel(unsigned int aNumChannel,
                                     std::string& aAudioFile)
{
   const Elocution* lCurrEloc = getCurrentEloc();

   if (aNumChannel >= lCurrEloc->mAudioFiles.size())
   {
      Log::error("Audio not found for channel [%u]", aNumChannel);
      aAudioFile = std::string();
   }

   aAudioFile = lCurrEloc->mAudioFiles[aNumChannel];
}

// Allows getting the current list of music files.
const std::vector<std::string>& Model::getCurrentMusics()
{
   const Stop* lCurrStop = getCurrentStop();
   return lCurrStop->mMusics;
}

bool Model::gpsMatch(double aGpsLat, double aGpsLon, double aGpsOri, char aNS, char aWE)
{
   unsigned int lFirstIndex, lLastIndex;

   static int lTotalNumElocs = mAllElocs.size();

   // getting the elocution number within the whole route.
   int lCurrentAbsEloc =
         mRoutes[mBookMark.mRoute]->mStops[mBookMark.mStop]->mElocs[mBookMark.mEloc]->mAbsElocIndex;

   if (lCurrentAbsEloc < mMarginGpsElocs)
   {
      lFirstIndex = 0;
   }
   else
   {
      lFirstIndex = lCurrentAbsEloc - mMarginGpsElocs;
   }

   if ((lCurrentAbsEloc + mMarginGpsElocs) > lTotalNumElocs)
   {
      lLastIndex = lTotalNumElocs - 1;
   }
   else
   {
      lLastIndex = lCurrentAbsEloc + mMarginGpsElocs;
   }

   double lDistLat, lDistLon, lDistOri;

   for (unsigned int i = lFirstIndex; i <= lLastIndex; i++)
   {
      const Elocution* lpEloc = mAllElocs[i];

      if (lpEloc != nullptr)
      {
         LocucioGPS* lpElocGpsInfo = lpEloc->mpGpsInfo;

         if (lpElocGpsInfo != nullptr)
         {
            lDistLat = std::abs(aGpsLat - lpElocGpsInfo->latitud);
            lDistLon = std::abs(aGpsLon - lpElocGpsInfo->longitud);
            lDistOri = std::abs(aGpsOri - lpElocGpsInfo->direccio);

            // Checking if position and direction are within the valid margins
            if (lDistLat < lpElocGpsInfo->marge_lat
                  && lDistLon < lpElocGpsInfo->marge_long
                  && lDistOri < lpElocGpsInfo->marge_direccio
                  && aNS == lpElocGpsInfo->NS && aWE == lpElocGpsInfo->WE)
            {
               // update the book mark that points to the next eloc to play
               Model::moveBookMarkToEloc(*lpEloc);
               return true;
            }
         }
      }
   }

   return false;
}

// Moves the book mark to the elocutions passed as a parameter.
// This method is for GPS purposes.
// The next elocution to be played will be the one indicated by the bookmark
// related to 'aEloc'.
void Model::moveBookMarkToEloc(const Elocution& aEloc)
{

   if (aEloc.mpParent == nullptr) // unlike to happen but just in case...
   {
      Log::error("Model: aEloc.mpParent == nullptr [%s]", aEloc.mElocutionName.c_str());
      return;
   }

   if (aEloc.mpParent->mpParent == nullptr)
   {
      Log::error("Model: aEloc.mpParent->mpParent == nullptr [%s]", aEloc.mElocutionName.c_str());
      return;
   }

   mBookMark.mRoute = aEloc.mpParent->mpParent->mRouteIndex;
   mBookMark.mStop = aEloc.mpParent->mStopIndex;
   mBookMark.mEloc = aEloc.mElocIndex;
}
